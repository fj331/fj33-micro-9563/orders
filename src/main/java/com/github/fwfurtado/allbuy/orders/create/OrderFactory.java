package com.github.fwfurtado.allbuy.orders.create;

import com.github.fwfurtado.allbuy.orders.shared.clients.ProductClient;
import com.github.fwfurtado.allbuy.orders.shared.domain.Order;
import org.springframework.stereotype.Component;

@Component
public class OrderFactory {


    private final ProductClient productClient;

    public OrderFactory(ProductClient productClient) {
        this.productClient = productClient;
    }

    public Order factoryOrderBy(OrderForm form) {
        var order = new Order(form.getCustomerId());

        form.getItems().forEach(item -> {
           var product = productClient.findProductById(item.getProductId());
           order.addItem(product, item.getQuantity());
        });

        return order;
    }
}
