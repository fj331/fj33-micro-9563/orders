package com.github.fwfurtado.allbuy.orders.shared.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long customerId;

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    private LocalDate issueDate;

    @ElementCollection
    @CollectionTable(name="order_items")
    private List<OrderItem> items = new ArrayList<>();

    Order() {
    }

    public Order(Long customerId) {
        this.customerId = customerId;
        this.issueDate = LocalDate.now();
        this.status = OrderStatus.WAITING_FOR_PAYMENT;
    }

    public Long getId() {
        return id;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void addItem(Product product, Long quantity) {
        items.add(new OrderItem(product, quantity));
    }

    public BigDecimal getTotal() {
        return items.stream().map(OrderItem::getTotal).reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}
