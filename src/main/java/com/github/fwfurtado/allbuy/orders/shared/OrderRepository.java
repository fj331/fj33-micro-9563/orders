package com.github.fwfurtado.allbuy.orders.shared;

import com.github.fwfurtado.allbuy.orders.create.CreationRepository;
import com.github.fwfurtado.allbuy.orders.shared.domain.Order;
import org.springframework.data.repository.Repository;

public interface OrderRepository extends Repository<Order, Long>, CreationRepository {
}
