package com.github.fwfurtado.allbuy.orders.create;

import org.springframework.stereotype.Service;

@Service
public class CreateService {
    private final OrderFactory factory;
    private final CreationRepository repository;

    public CreateService(OrderFactory factory, CreationRepository repository) {
        this.factory = factory;
        this.repository = repository;
    }

    public Long createOrderBy(OrderForm form) {
        ensureThatThereEnoughStock(form);

        var order = factory.factoryOrderBy(form);
        repository.save(order);

        return order.getId();
    }

    private void ensureThatThereEnoughStock(OrderForm form) {

    }
}
