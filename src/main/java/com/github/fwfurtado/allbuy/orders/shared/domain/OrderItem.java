package com.github.fwfurtado.allbuy.orders.shared.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Embeddable
public class OrderItem {

    @Embedded
    @AttributeOverrides(@AttributeOverride(name="id",column=@Column(name="product_id")))
    private Product product;

    private Long quantity;

    OrderItem() {
    }

    public OrderItem(Product product, Long quantity) {
        this.product = product;
        this.quantity = quantity;
    }


    public Product getProduct() {
        return product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotal() {
        return product.getPriceForQuantity(quantity);
    }
}
