package com.github.fwfurtado.allbuy.orders.create;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;

import static org.springframework.http.ResponseEntity.created;

@RestController
@RequestMapping("orders")
public class CreateController {

    private final CreateService service;

    public CreateController(CreateService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> create(@RequestBody OrderForm form) {
        var id = service.createOrderBy(form);
        var uri = URI.create("/orders/" + id.toString());

        return created(uri).build();
    }
}
