package com.github.fwfurtado.allbuy.orders.shared.clients;

import com.github.fwfurtado.allbuy.orders.shared.domain.Product;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name="products", url="http://localhost:8090/products")
public interface ProductClient {

    @GetMapping("{id}")
    Product findProductById(@PathVariable Long id);
}
