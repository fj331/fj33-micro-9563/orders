package com.github.fwfurtado.allbuy.orders.create;

import java.util.ArrayList;
import java.util.List;

public class OrderForm {

    private Long customerId;
    private List<OrderItemForm> items = new ArrayList<>();

    public Long getCustomerId() {
        return customerId;
    }

    public List<OrderItemForm> getItems() {
        return items;
    }

    static class OrderItemForm {
        private Long productId;
        private Long quantity;

        public Long getProductId() {
            return productId;
        }

        public Long getQuantity() {
            return quantity;
        }
    }
}
