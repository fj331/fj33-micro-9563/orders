package com.github.fwfurtado.allbuy.orders.shared.domain;

public enum OrderStatus {
    WAITING_FOR_PAYMENT, PAID,
    WAITING_FOR_SHIPPING, SHIPPED,
    RECEIVED, CANCELED
}
