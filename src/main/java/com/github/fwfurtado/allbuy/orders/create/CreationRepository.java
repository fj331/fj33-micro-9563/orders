package com.github.fwfurtado.allbuy.orders.create;

import com.github.fwfurtado.allbuy.orders.shared.domain.Order;

public interface CreationRepository {
    void save(Order order);
}
