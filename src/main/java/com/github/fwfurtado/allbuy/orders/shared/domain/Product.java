package com.github.fwfurtado.allbuy.orders.shared.domain;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

@Embeddable
public class Product {
    private Long id;
    private BigDecimal price;

    Product() {
    }

    public Product(Long id, BigDecimal price) {
        this.id = id;
        this.price = price;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public BigDecimal getPriceForQuantity(Long quantity) {
        return BigDecimal.valueOf(quantity).multiply(price);
    }
}
